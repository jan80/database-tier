docker build -t beresneva/database-tier .
docker stop database-tier
docker rm database-tier

docker run -p 3306:3306 -d --name database-tier beresneva/database-tier
